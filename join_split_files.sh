#!/bin/bash

cat vendor/lib64/librelight_only.so.* 2>/dev/null >> vendor/lib64/librelight_only.so
rm -f vendor/lib64/librelight_only.so.* 2>/dev/null
cat product/data-app/MIMediaEditor/MIMediaEditor.apk.* 2>/dev/null >> product/data-app/MIMediaEditor/MIMediaEditor.apk
rm -f product/data-app/MIMediaEditor/MIMediaEditor.apk.* 2>/dev/null
cat product/data-app/Health/Health.apk.* 2>/dev/null >> product/data-app/Health/Health.apk
rm -f product/data-app/Health/Health.apk.* 2>/dev/null
cat product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null >> product/data-app/MiuiScanner/MiuiScanner.apk
rm -f product/data-app/MiuiScanner/MiuiScanner.apk.* 2>/dev/null
cat product/app/MiuiBiometric33148/MiuiBiometric33148.apk.* 2>/dev/null >> product/app/MiuiBiometric33148/MiuiBiometric33148.apk
rm -f product/app/MiuiBiometric33148/MiuiBiometric33148.apk.* 2>/dev/null
cat product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null >> product/app/TrichromeLibrary64/TrichromeLibrary64.apk
rm -f product/app/TrichromeLibrary64/TrichromeLibrary64.apk.* 2>/dev/null
cat product/app/MIpay/MIpay.apk.* 2>/dev/null >> product/app/MIpay/MIpay.apk
rm -f product/app/MIpay/MIpay.apk.* 2>/dev/null
cat product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null >> product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk
rm -f product/app/VoiceAssistAndroidT/VoiceAssistAndroidT.apk.* 2>/dev/null
cat product/app/MiGameService_MTK/MiGameService_MTK.apk.* 2>/dev/null >> product/app/MiGameService_MTK/MiGameService_MTK.apk
rm -f product/app/MiGameService_MTK/MiGameService_MTK.apk.* 2>/dev/null
cat product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null >> product/app/WebViewGoogle64/WebViewGoogle64.apk
rm -f product/app/WebViewGoogle64/WebViewGoogle64.apk.* 2>/dev/null
cat product/app/CarWith/CarWith.apk.* 2>/dev/null >> product/app/CarWith/CarWith.apk
rm -f product/app/CarWith/CarWith.apk.* 2>/dev/null
cat product/app/HybridPlatform/HybridPlatform.apk.* 2>/dev/null >> product/app/HybridPlatform/HybridPlatform.apk
rm -f product/app/HybridPlatform/HybridPlatform.apk.* 2>/dev/null
cat product/app/OtaProvision/OtaProvision.apk.* 2>/dev/null >> product/app/OtaProvision/OtaProvision.apk
rm -f product/app/OtaProvision/OtaProvision.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/priv-app/MIUIMusicT/MIUIMusicT.apk.* 2>/dev/null >> product/priv-app/MIUIMusicT/MIUIMusicT.apk
rm -f product/priv-app/MIUIMusicT/MIUIMusicT.apk.* 2>/dev/null
cat product/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null >> product/priv-app/MIUIVideo/MIUIVideo.apk
rm -f product/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null
cat product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null >> product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk
rm -f product/priv-app/MiuiExtraPhoto/MiuiExtraPhoto.apk.* 2>/dev/null
cat product/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null >> product/priv-app/MIUIBrowser/MIUIBrowser.apk
rm -f product/priv-app/MIUIBrowser/MIUIBrowser.apk.* 2>/dev/null
cat product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> product/priv-app/MiuiCamera/MiuiCamera.apk
rm -f product/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat product/priv-app/Gallery_T_CN/Gallery_T_CN.apk.* 2>/dev/null >> product/priv-app/Gallery_T_CN/Gallery_T_CN.apk
rm -f product/priv-app/Gallery_T_CN/Gallery_T_CN.apk.* 2>/dev/null
cat system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null >> system_ext/apex/com.android.vndk.v30.apex
rm -f system_ext/apex/com.android.vndk.v30.apex.* 2>/dev/null
cat system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null >> system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk
rm -f system_ext/priv-app/MiuiFreeformService/MiuiFreeformService.apk.* 2>/dev/null
cat system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system_ext/priv-app/Settings/Settings.apk
rm -f system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat bootimg/01_dtbdump_MT6893.dtb.* 2>/dev/null >> bootimg/01_dtbdump_MT6893.dtb
rm -f bootimg/01_dtbdump_MT6893.dtb.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
